package it.prowebcam.prowebcam.prowebcam;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Contatti extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatti);
        init();
    }
    public void init(){
        FloatingActionButton fabPhone = findViewById(R.id.fabPhone);
        FloatingActionButton fabEmail = findViewById(R.id.fabEmail);
        fabPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCall = new Intent(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse(getString(R.string.phone_intent)));
                startActivity(intentCall);
            }
        });
        fabEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSendEmail  = new Intent(Intent.ACTION_SEND);
                intentSendEmail.setData(Uri.parse(getString(R.string.email_setData)));
                String[] to = {getString(R.string.email)};
                intentSendEmail.putExtra (Intent.EXTRA_EMAIL, to);
                intentSendEmail.setType(getString(R.string.email_setType));
                startActivity(Intent.createChooser(intentSendEmail,getString(R.string.intent_email_title)));
            }
        });
    }
}
