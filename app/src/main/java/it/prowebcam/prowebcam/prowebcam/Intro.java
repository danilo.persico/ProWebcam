package it.prowebcam.prowebcam.prowebcam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class Intro extends AppCompatActivity {
    private static final long SPLASH_TIME = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
       init();
    }
    public void init(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent intentWebcam = new Intent().setClass(Intro.this,Webcam.class);
                startActivity(intentWebcam);
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_TIME);
    }
}
