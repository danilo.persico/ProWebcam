package it.prowebcam.prowebcam.prowebcam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import it.prowebcam.prowebcam.prowebcam.model.CamModel;


public class Webcam extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String WEBCAM_LIST_URL = "https://www.prowebcam.it/app/webcam-list.php";
    private static final String ACCESS_KEY = "pR0w3bc4M111";
    private static final String NAME = "name";
    private static final String LOCATION = "location";
    private static final String LOCATION_DETAIL = "location_detail";
    private static final String PLAY = "https://play.google.com/store/apps/details?id=it.prowebcam.prowebcam.prowebcam&hl=it";
    private ListView lsvWebcam;
    private ProgressDialog dialog;
    private TextView tvRandom;
    private WebView webcam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webcam);
        init();
    }

    private void createWebcamList(JSONArray jsonArray) {
        final List<CamModel> webcams = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject currentJsonObject = jsonArray.getJSONObject(i);
                final CamModel webcam = new CamModel(currentJsonObject);
                webcam.setName(currentJsonObject.getString(NAME));
                webcam.setLocation(currentJsonObject.getString(LOCATION));
                webcam.setLocation_detail(currentJsonObject.getString(LOCATION_DETAIL));
                webcam.setImage("https://www.prowebcam.it/webcam/image/" + webcam.getName());
                webcams.add(webcam);
                Random rand = new Random();
                int number = rand.nextInt(10) + 1;
                String myString = String.valueOf(number);
                tvRandom.setText(myString);
                int indice = number;
                switch (indice) {
                    case 1:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/santommaso");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 2:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/caramanico");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 3:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/ortona1");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 4:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/ortona2");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 5:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/chieti");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 6:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/voltigno");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 7:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/chieti2");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 8:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/civitella");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 9:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/castelli");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                    case 10:
                        this.webcam.getSettings().setJavaScriptEnabled(true);
                        this.webcam.getSettings().setSupportZoom(true);
                        this.webcam.getSettings().setBuiltInZoomControls(true);
                        this.webcam.getSettings().setLoadWithOverviewMode(true);
                        this.webcam.getSettings().setUseWideViewPort(true);
                        this.webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        this.webcam.loadUrl("https://www.prowebcam.it/webcam/image/hotelcaravel");
                        this.webcam.setWebViewClient(new WebViewClient());
                        break;
                }
            } catch (JSONException e) {
                continue;
            }
        }
        Toast.makeText(this, getString(R.string.list_loaded), Toast.LENGTH_SHORT).show();
        lsvWebcam = findViewById(R.id.lvWebcam);
        CamAdapter adapter = new CamAdapter(getApplicationContext(), R.layout.row, webcams);
        adapter.notifyDataSetChanged();
        lsvWebcam.setAdapter(adapter);
        Collections.sort(webcams);
        lsvWebcam.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CamModel camModel = (CamModel) lsvWebcam.getItemAtPosition(position);
                webcam.getSettings().setJavaScriptEnabled(true);
                webcam.getSettings().setSupportZoom(true);
                webcam.getSettings().setBuiltInZoomControls(true);
                webcam.getSettings().setLoadWithOverviewMode(true);
                webcam.getSettings().setUseWideViewPort(true);
                webcam.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                webcam.loadUrl("https://www.prowebcam.it/webcam/image/" + camModel.getName());
                webcam.setWebViewClient(new WebViewClient());
            }
        });
    }

    public void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        webcam = findViewById(R.id.wvWebcam);
        tvRandom = findViewById(R.id.tvRandom);
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.dialog_msg));
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        RequestQueue queue = Volley.newRequestQueue(this);
        try {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                    Request.Method.POST,
                    WEBCAM_LIST_URL,
                    new JSONArray("[{\"appKey\":\"" + ACCESS_KEY + "\"}]"),
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray jsonArray) {
                            createWebcamList(jsonArray);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(Webcam.this, getString(R.string.error_load_webcam), Toast.LENGTH_LONG).show();
                        }
                    }
            );
            queue.add(jsonArrayRequest);
        } catch (JSONException e) {
            Toast.makeText(Webcam.this, getString(R.string.error_load_webcam), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public class CamAdapter extends ArrayAdapter {
        private List<CamModel> webModelList;
        private int resource;
        private LayoutInflater inflater;

        public CamAdapter(Context context, int resource, List<CamModel> objects) {
            super(context, resource, objects);
            webModelList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.cam = convertView.findViewById(R.id.imvCamera);
               // holder.name = convertView.findViewById(R.id.name);
                holder.location = convertView.findViewById(R.id.tvLocation);
                holder.location_detail = convertView.findViewById(R.id.tvLocation_detail);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final ViewHolder finalHolder = holder;
            holder.location.setText(webModelList.get(position).getLocation());
            holder.location_detail.setText(webModelList.get(position).getLocation_detail());
            return convertView;
        }

        class ViewHolder {
            private ImageView cam;
            private TextView name;
            private TextView location;
            private TextView location_detail;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.webcam, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.chi_siamo) {
            Intent intentChiSiamo = new Intent(getApplicationContext(), Siamo.class);
            startActivity(intentChiSiamo);
        } else if (id == R.id.scopo) {
            Intent intentScopo = new Intent(getApplicationContext(), Scopo.class);
            startActivity(intentScopo);
        } else if (id == R.id.contatti) {
            Intent intentContatti = new Intent(getApplicationContext(), Contatti.class);
            startActivity(intentContatti);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void share() {
        Intent shareintent = new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT, PLAY);
        startActivity(Intent.createChooser(
                shareintent, getString(R.string.share)));
    }
}

