package it.prowebcam.prowebcam.prowebcam.model;

import org.json.JSONObject;

/**
 * Created by Danale on 28/04/2017.
 */
public class CamModel implements Comparable<CamModel> {
    public String name;
    public String location;
    public String location_detail;
    public String image;

    public CamModel(JSONObject currentJsonObject) {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation_detail() {
        return location_detail;
    }

    public void setLocation_detail(String location_detail) {
        this.location_detail = location_detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    @Override
    public int compareTo(CamModel webcam) {
        return this.location.compareTo(webcam.getLocation());
    }
}




